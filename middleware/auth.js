const { UserModel } = require("../models");

const config = require("../config");
const jwt = require("jsonwebtoken");

module.exports = async (req, res, next) => {
  try {
    const tokenHeader = req.headers["bearer"];
    const decodedToken = jwt.verify(tokenHeader, config.JWT_SECRET_KEY);

    const findUserToken = await UserModel.findOne({ token: tokenHeader });

    if (!findUserToken)
      return res
        .status(401)
        .json({ status: 401, message: "Token is not match with any users" })
        .end();

    return next();
  } catch (error) {
    console.error("Error occured with message :", error);

    let errorMsg = error.message;
    let errorSts = 500;

    if (errorMsg === "jwt must be provided") {
      errorSts = 403;
      errorMsg = "Token jwt is mandatory, please insert the token first";
    } else if (error.message === "invalid signature") {
      errorSts = 400;
      errorMsg = "Please insert a correct jwt token";
    }

    res.status(errorSts).json({ status: errorSts, message: errorMsg }).end();
  }
};
