const router = require("express").Router();
const controller = require("./controller");

const { validateAll, rulesLogin, rulesRegister } = require("../../helpers");

router.post(
  "/user/register",
  [rulesRegister(), validateAll],
  controller.userRegister
);
router.post("/user/login", [rulesLogin(), validateAll], controller.userLogin);

module.exports = router;
