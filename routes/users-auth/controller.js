const { UserModel } = require("../../models");
const {
  hashPassword,
  comparedPassword,
  generateToken,
} = require("../../helpers");

module.exports = {
  userRegister: async (req, res) => {
    const hasPass = await hashPassword(req.body.password);
    const genToken = await generateToken(req.body);

    try {
      //Check if email already used
      const userCheckEmail = await UserModel.findOne({ email: req.body.email });

      if (userCheckEmail)
        return res.status(406).json({
          status: 406,
          message: `User with email ${req.body.email} already used, please register with another email`,
        });

      //Continue registration process
      const userRegistration = await UserModel.create({
        ...req.body,
        password: hasPass,
        token: genToken,
      });

      const { _id, name, email } = userRegistration;

      res.status(201).json({
        status: 201,
        message: `User successfully created with id ${userRegistration._id}`,
        data: { _id, name, email },
      });
    } catch (error) {
      console.error("Error occured with message :", error);
      res.status(500).json({ status: 500, message: error.message });
    }
  },
  userLogin: async (req, res) => {
    try {
      await UserModel.findOne({
        email: req.body.email,
      }).then(async (user) => {
        if (!user)
          return res.status(404).json({
            status: 404,
            message: `User with email ${req.body.email} not found`,
          });

        const comparePass = await comparedPassword(
          req.body.password,
          user.password
        );

        if (!comparePass)
          return res.status(401).json({
            status: 401,
            message: "The password that entered was incorrect",
          });

        //Successfully validation
        const { name, email, token } = user;

        return res.status(200).json({
          status: 200,
          message: "Login succesfully",
          data: { name, email, token },
        });
      });
    } catch (error) {
      console.error("Error occured with message :", error);
      res.status(500).json({ status: 500, message: error.message }).end();
    }
  },
};
