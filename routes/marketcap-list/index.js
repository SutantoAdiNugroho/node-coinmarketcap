const router = require("express").Router();
const controller = require("./controller");

const { authMiddleware } = require("../../middleware");

router.get("/cryptocurrency/map", authMiddleware, controller.getIDMap);
router.get(
  "/cryptocurrency/listings/latest",
  authMiddleware,
  controller.getListingsLatest
);
router.get(
  "/cryptocurrency/quotes/latest",
  authMiddleware,
  controller.getQuotesLatest
);

module.exports = router;
