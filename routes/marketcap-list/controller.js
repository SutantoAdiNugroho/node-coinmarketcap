const { axiosMarketCap } = require("../../helpers");

let urlMain = "";

module.exports = {
  getIDMap: async (req, res) => {
    try {
      urlMain = "cryptocurrency/map";

      const returnAxios = await axiosMarketCap(urlMain, req.query);

      return res.status(200).json({
        status: 200,
        message:
          "Succesfully returns a mapping of all cryptocurrencies to unique CoinMarketCap ids",
        data: returnAxios,
      });
    } catch (error) {
      console.error("Error occured with message :", error);

      const axiosResp = error.response;
      if (axiosResp !== undefined) {
        const statusCd = error.response.data.status.error_code;
        const errMsg = error.response.data.status.error_message;

        res.status(statusCd).json({ status: statusCd, message: errMsg }).end();
      } else {
        res.status(500).json({ status: 500, message: error.message }).end();
      }
    }
  },
  getListingsLatest: async (req, res) => {
    try {
      urlMain = "cryptocurrency/listings/latest";

      const returnAxios = await axiosMarketCap(urlMain, req.query);

      return res.status(200).json({
        status: 200,
        message:
          "Succesfully returns a paginated list of all active cryptocurrencies with latest market data",
        data: returnAxios,
      });
    } catch (error) {
      console.error("Error occured with message :", error);

      const axiosResp = error.response;
      if (axiosResp !== undefined) {
        const statusCd = error.response.data.status.error_code;
        const errMsg = error.response.data.status.error_message;

        res.status(statusCd).json({ status: statusCd, message: errMsg }).end();
      } else {
        res.status(500).json({ status: 500, message: error.message }).end();
      }
    }
  },
  getQuotesLatest: async (req, res) => {
    try {
      urlMain = "cryptocurrency/quotes/latest";

      const returnAxios = await axiosMarketCap(urlMain, req.query);

      return res.status(200).json({
        status: 200,
        message: `Succesfully returns the latest market quote`,
        data: returnAxios,
      });
    } catch (error) {
      console.error("Error occured with message :", error);

      const axiosResp = error.response;
      if (axiosResp !== undefined) {
        const statusCd = error.response.data.status.error_code;
        const errMsg = error.response.data.status.error_message;

        res.status(statusCd).json({ status: statusCd, message: errMsg }).end();
      } else {
        res.status(500).json({ status: 500, message: error.message }).end();
      }
    }
  },
};
