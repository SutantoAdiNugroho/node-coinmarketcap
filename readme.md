## CoinMarketCap Rest API

A simple rest api used to display the latest cryptocurrency from [CoinMarketCap.](https://coinmarketcap.com/api/documentation/v1/)

### How to run the apps :

#### 1. Clone the repository
```
$ git clone https://SutantoAdiNugroho@bitbucket.org/SutantoAdiNugroho/node-coinmarketcap.git
```
#### 2. Install dependencies
```
$ cd node-coinmarketcap
```
and then
```
$ npm install
```
#### 3. Configure .env file on the project
There is already a sample env file, and all the keywords from the env file must be filled in.

| Keywords        | Description                      |
| ----------------|----------------------------------|
| HOST_DB         | MongoDB connection               |
| PORT            | Port to run the apps             |
| MARKETCAP_KEY   | The CoinMarketCap API Key        |
| JWT_SECRET_KEY  | Key for build and sync JWT token |

#### 4. Launch the apps
Example command for running it locally :
```
$ npm run dev
```

After the apps running is succesfully, we can start by calling routes. For example :

![Alt text](./src/images/1-1-runapps.png "Calling '/' route")

### API Guide And Documentation
#### /api/auth

| Path           | Method | Description   |
| ---            | -----  | ----          |
| /user/login    | POST   | User login    |
| /user/register | POST   | User register |

#### /api/coinmarketcap

| Path                            | Method | Description   |
| ---                             | -----  | ----          |
| /cryptocurrency/map             | GET    | Showing a mapping of all cryptocurrencies    |
| /cryptocurrency/listings/latest | GET    | Showing a paginated list of all active cryptocurrencies with latest market data |
| /cryptocurrency/quotes/latest   | GET    | Showing the latest market quote for 1 or more cryptocurrencies |

* ####  Register
    
    * Access URL : /api/auth/user/register 
    * Method     : POST
    * Parameters :
    
        | Name     | Type     | Description   |
        | ---      | -----    | ----          |
        | name     | string   | Name of user  |
        | email    | string   | Email of user, the value must be email format |
        | password | string   | Password of user, the length value must be 5 characters or more |
        
    * Example success response :
        ![Alt text](./src/images/2-1-register.png "Calling register route")

* ####  Login

    * Access URL : /api/auth/user/login 
    * Method     : POST
    * Parameters :
    
        | Name     | Type     | Description                                   |
        | ---      | -----    | ----                                          |
        | email    | string   | Email of user, the value must be email format |
        | password | string   | Password of user                              |
        
    * Example success response :
        ![Alt text](./src/images/2-2-login.png "Calling login route")

    * Example password incorrect response :
        ![Alt text](./src/images/2-3-login.png "Calling login route")

* ####  CoinMarketCap ID Map

    * Access URL : /api/coinmarketcap/cryptocurrency/map
    * Method     : GET
    * Additional : Token of user is needed when execute this endpoint
    * Query parameters :
    
        | Name            | Type      | Description                                               |
        | ---             | -----     | ----                                                      |
        | listing_status  | string    | Pass ``active`` will show only active cryptocurrencies, or pass ``inactive`` will show cryptocurrencies that are no longer active. And pass ``untracked`` to get a list of cryptocurrencies that are listed but do not yet meet methodology requirements to have tracked markets available  |
        | start           | integer   | Optionally set paginated list of items                    |
        | limit           | integer   | Optionally specify the number of results to return        |
        
    * Example success response :
        ![Alt text](./src/images/2-1-map.png "Calling map route")
    
    * Example success response with query :
        ![Alt text](./src/images/2-2-map.png "Calling map route")

    * Example error response without token :
        ![Alt text](./src/images/2-3-map.png "Calling map route")

* ####  List crypto with latest market data

    * Access URL : /api/coinmarketcap/cryptocurrency/listings/latest
    * Method     : GET
    * Additional : Token of user is needed when execute this endpoint
    * Query parameters :
    
        | Name                    | Type      | Description                                               |
        | ---                     | -----     | ----                                                      |
        | start                   | integer   | Optionally set paginated list of items                    |
        | limit                   | integer   | Optionally specify the number of results to return        |
        | price_min               | number    | Optionally filter by minimum USD price                    |
        | price_max               | number    | Optionally filter by maximum USD price                    |
        | market_cap_min          | number    | Optionally filter by minimum market cap                   |
        | market_cap_max          | number    | Optionally filter by maximum market cap                   |
        | volume_24h_min          | number    | Optionally filter by minimum 24 hour USD volume           |
        | volume_24h_max          | number    | Optionally filter by maximum 24 hour USD volume           |
        | circulating_supply_min  | number    | Optionally filter by minimum circulating supply           |
        | circulating_supply_max  | number    | Optionally filter by maximum circulating supply           |
        | percent_change_24h_min  | number    | Optionally filter by minimum 24 hour percent change       |
        | percent_change_24h_max  | number    | Optionally filter by maximum 24 hour percent change       |
        
    * Example success response :
        ![Alt text](./src/images/2-1-listings.png "Calling listings latest route")
    
    * Example success response with query :
        ![Alt text](./src/images/2-2-listings.png "Calling listings latest route")

    * Example error response without token :
        ![Alt text](./src/images/2-3-listings.png "Calling listings latest route")

* ####  Latest market quote

    * Access URL : /api/coinmarketcap/cryptocurrency/quotes/latest
    * Method     : GET
    * Additional : Token of user is needed when execute this endpoint
    * Query parameters : At least one "id" or "slug" or "symbol" is required for this endpoint.
    
        | Name                    | Type      | Description                                               |
        | ---                     | -----     | ----                                                      |
        | id                      | string    | Mandatory CoinMarketCap ID, example: 1,2                  |
        | slug                    | string    | Alternative cryptocurrency slug, example: bitcoin         |
        | symbol                  | string    | Alternative cryptocurrency symbol, example: BTC, ETH      |
        
    * Example success response :
        ![Alt text](./src/images/2-1-quote.png "Calling quote route")
    
    * Example error response without query :
        ![Alt text](./src/images/2-2-quote.png "Calling quote route")

    * Example error response without token :
        ![Alt text](./src/images/2-3-quote.png "Calling quote route")