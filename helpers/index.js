const hashPassword = require("./password/hash");
const comparedPassword = require("./password/compared");
const generateToken = require("./token/token");
const axiosMarketCap = require("./axios/axios");

const validateAll = require("./validators/validate");
const { rulesLogin, rulesRegister } = require("./validators/authentication");

module.exports = {
  hashPassword: hashPassword,
  comparedPassword: comparedPassword,
  generateToken: generateToken,
  validateAll: validateAll,
  rulesLogin: rulesLogin,
  rulesRegister: rulesRegister,
  axiosMarketCap: axiosMarketCap,
};
