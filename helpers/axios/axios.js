const axios = require("axios");
const config = require("../../config");

const getMarketCapInfo = async (urlMain, params) => {
  const axiosAct = await axios({
    method: "GET",
    url: `https://pro-api.coinmarketcap.com/v1/${urlMain}`,
    headers: { "X-CMC_PRO_API_KEY": config.MARKETCAP_KEY },
    params: params,
  }).then((response) => {
    return response.data.data;
  });

  return axiosAct;
};

module.exports = getMarketCapInfo;
