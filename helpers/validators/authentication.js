const { body } = require("express-validator");

const rulesLogin = () => {
  return [body("email").isEmail()];
};

const rulesRegister = () => {
  return [body("email").isEmail(), body("password").isLength({ min: 5 })];
};

module.exports = { rulesLogin, rulesRegister };
